ИГРА Арканойд!! добро пожаловать!!

**********************************
Имя игры: 		Ships Battle
Версия: 		1.0.0
Создатель: 		Heavenly Waltz
**********************************


////////////////////////////////////////////////////////
Создано на:
-----------
Язык: 			C++
Компилятор: 		mingw-w64
Среда: 			Notepade++ (+ установленный плагин NppExec)
Платформа:		Windows 10

Библиотеки:
			SDL2-2.24.0
			SDL2_image-2.6.2
			SDL2_mixer-2.6.2
			SDL2_ttf-2.20.1



///////////////////////////////////////////////////////
Структура:
----------
 inc.h 			// тут всё подключим что нужно, "нелюблю много инклюдов в главном файле"
 Ships_Battle.cpp 	// Главный файл с функцией main
 info.rc 		// Немножечка информации об игре авторе и иконки для .exe ;)
____________________
 core[] 		// ПАПКА ядра для сборки игры
 - game_settings.h 	// Тут находятся ГЛОБАЛЬНО общие параметры для игры ( скорость шарика, корабля и т.д )
 - variables.h 		// Тут находятся ГЛОБАЛЬНЫЕ классы и объекты, для работы с игрой 

 - prototypes[] 	// ПАПКА внутри котороый находятся заголовочные файлы ( .h ) для протатипов игры.
 -- classes.h 		// объявление всех нужных классов ( да да в 1м файле их нетак много) 
 -- functions.h 	// объявление всех нужных функций( да да в 1м файле их нетак много) 

 - classes[] 		// ПАПКА Внутри находятся ОБЪЕКТЫ, они-же классы. для игры. ( объект корабля, шарика, сундука.... )
 -- ball.cpp 		// Описание логик и функция для летающего ядра (  мяча )
 -- chest.cpp 		// Объект с  ЯЩИКАМИ, он их и рисует и удоляет и колизию с мячом проверяет
 -- controller.cpp 	// Тот  самый плавающий снизу кораблик ) мячик ловит и не ловит как повезёт
 -- LTexture.cpp 	// Объект для загрузки различных ресурсов игры "текста и картинок"

 - functions[] 		// ПАПКА Внутри  расположенны различные функции для удобства
 -- init.cpp 		// Функция инициализатор ( запускатор ) для всего проекта и игры. всяки проверки и загрузки
 -- loadMedia.cpp 	// Загружает все медиа ресурсы для игры ( картинки, ауди, шрифты )
 -- supporting.cpp 	// небольшие функции помошники не требующии больших объёмов кода
 -- close.cpp		// Очищает память по закрытию игры, и все её ресуры
____________________
 data[] 		// ПАПКА для хранения различных ресурсов
 - fonts[] 		// ПАПКА тут хранятся шрифты
 - img[] 		// ПАПКА тут хранить картинки, спрайты и т.д
 - sounds[] 		// ПАПКА хранит всякие звуки в формате .wav
____________________
 lib[] 			// Папка для релиза, если вам надо передать игру на другой ПК,  Подробней чуть ниже раздел "Релиз"



///////////////////////////////////////////////////////
Сборка:
----------
Чтобы собрать игру вам потребуется всё что перечислено в  "Создана на"
После того как установили всё необходимое и проверели что оно работает, и да да незабыли прописать path в windows.
1) открываем в "Notepad++"  основной файл игры "Ships_Battle.cpp"
2) открываем плагин ( обычно доступно просто на F6 ) "NppExec" в "Notepad++" ( сверху в меню раздел "плагины") в плагине  выбираем "Execute NppExex Script" 
3) Выбираем любой скрипт ( приложенны ниже "раздел Релиз и Дебаг" ) и запускаем.
4) в папке рядом с "Ships_Battle.cpp" должен появится "Ships_Battle.exe".
5) если хотим передать игру на другой ПК, то смотрим. Релиз -> ( 2 );



///////////////////////////////////////////////////////
Релиз:
----------
Для релиза необходимо.
( 1 ) Скрипт ( для NppExec ):
____________________________________
npp_save
cd "$(CURRENT_DIRECTORY)"
windres info.rc -O coff -o my.res
g++ -optl-static -lstdc++ -static-libgcc -static-libstdc++ $(FILE_NAME) core/functions/*.cpp core/classes/*.cpp -w -Wl,-subsystem,windows -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -o $(NAME_PART) my.res
NPP_RUN $(NAME_PART)
_____________________________________

Запустит игру
ВНИМАНИЕ!!!!!!!!!!!!!!!!!!!!
Подключение файлов ".cpp" из папок "core/functions/" и "core/classes/" Происходит автоматически, ТО-ЕСТЬ ВСЕХ ЧТО ТАМ ЕСТЬ !!!!! будте осторожны

( 2 ) Передача на другой ПК
2.1 После того как вы получите свой "Ships_Battle.exe"
2.2 вы можете создать папку например "MyGame"
2.3 Разместить в ней папку "data" со всем её содержимым
2.4 из папки "lib" поместить все файлы рядом с "Ships_Battle.exe"

должна получится вот такая структура:
MyGame[]
 - Ships_Battle.exe
 - SDL2.dll
 - SDL2_image.dll
 - SDL2_ttf.dll
 - SDL2_mixer.dll
 - libwinpthread-1.dll
 - data[]
 -- fonts[] // там всякие шрифты перечислять нестану думаю понятно.
 -- img[] // там всякие картинки перечислять нестану думаю понятно.
 -- sounds[] // там всякие звуки перечислять нестану думаю понятно.



///////////////////////////////////////////////////////
Дебаг:
----------
Если что-то пошло нетак. и вам нужна дополнительная консоль от игры то.
Скрипт ( для NppExec ):
____________________________________
npp_save
cd "$(CURRENT_DIRECTORY)"
g++ $(FILE_NAME) core/functions/*.cpp core/classes/*.cpp -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -o $(NAME_PART)
NPP_RUN $(NAME_PART)
_____________________________________

Запустит игру в режиме отладки выводя дополнительную консоль для ваших нужд
ВНИМАНИЕ!!!!!!!!!!!!!!!!!!!!
Подключение файлов ".cpp" из папок "core/functions/" и "core/classes/" Происходит автоматически, ТОЕСТЬ ВСЕХ ЧТО ТАМ ЕСТЬ !!!!! будте осторожны