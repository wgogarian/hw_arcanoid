#pragma once
//Texture wrapper class
class LTexture {
	public:
		LTexture(); // конструктор 
		~LTexture(); // деструктор
		bool loadFromFile( std::string path ); 		//Loads image at specified path
		bool loadFromRenderedText( TTF_Font* gFont, std::string textureText, SDL_Color textColor );//Creates image from font string
		void free();//Deallocates texture
		void setColor( Uint8 red, Uint8 green, Uint8 blue ); //Set color modulation
		void setBlendMode( SDL_BlendMode blending ); //Set blending		
		void setAlpha( Uint8 alpha ); //Set alpha modulation
		void render( int x, int y, SDL_Rect* clip = NULL ); //Renders texture at given point
		//Gets image dimensions
		int getWidth();
		int getHeight();

	private:
		SDL_Texture* mTexture; //The actual hardware texture
		//Image dimensions
		int mWidth;
		int mHeight;
};

// контроллер для коробля
class  controller{
	public:
		controller(); // конструктор 
		~controller(); // деструктор
		void loadTexture( LTexture& textureLoad );
		void eventCheck( SDL_Event& e );
		void move();
		void render();
		int getPosX();
		int getVectX();
		int getWidth();
		
	private:
		int movX,starX,spriteY,spriteW,vectX;
		struct spriteAnim{
			int frame;
			int iter;
			int step;
		} sprite;
		LTexture texture;
		bool flag_move_a,flag_move_d;
};

// класс для ядра ( он же мячик что туда сюда литает )
class ball{
	public:	
		ball(); // конструктор 
		~ball(); // деструктор
		void loadTexture( LTexture& textureLoad );
		void eventCheck( SDL_Event& e );
		void move();
		void render();
		void restart();
		void reversX(); // Изменить полёт по кординате X
		void reversY(); // Изменить полёт по кординате Y
		void setPosXY(int x, int y); // Изменить полёт по кординате Y
		int getPosX(); // Вернёт текущею позиию X
		int getPosY(); // Вернёт текущий позиию Y
		int *getVectorXY();

	private:
		int posX,posY,vectX,vectY,controllerW;
		int vectArr[2];
		bool FLAY,sounfirstShot,soundEndSplash,flagEndGame;
		SDL_Rect rect;
		LTexture texture;
		controller control;
};

// сундуки и тамже их рендер
class chest{
	public:	
		chest(); // конструктор 
		~chest(); // деструктор
		void init(int LineChest, int SumLineChest);
		void colision(ball *cannon);
		void render();
		void clear();
		int getSumChest();
	private:
		int stepChestX,stepChestY,lifeChests,lineChest,sumLineChest;
		struct chestStruct{
			int x;
			int y;
			int sprite;
			bool life;
		} **chestD;
		ball *cannonB;
		SDL_Rect SpriteChest[13]; // спайты сундуков
};