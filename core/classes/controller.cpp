#include <SDL2\SDL.h>
#include <SDL2\SDL_ttf.h>

#include <iostream>
#include <string>
#include <sstream>

#include "../prototypes/classes.h"

// различные Глобальные переменные для работы
extern const int SCREEN_WIDTH; // ширина экрана
extern const int Y_POS_SHIPS; // Положение кораблика по высоте 

extern int G_speedShips; // скорость движения коробля ( начальные )
extern int G_speedAnimShips; // Скорость анимации при плавании коробля

// КОНТРОЛЬ КОРОБЛЯ
controller::controller(){
	vectX = 1;
	movX = 0;
	spriteW = 121;
	spriteY = 180;
	starX = (SCREEN_WIDTH / 2) - ( spriteW / 2 );
	sprite = {0,0,0};
	flag_move_a = false;
	flag_move_d = false;
}
controller::~controller(){}

void controller::loadTexture( LTexture& textureLoad ){texture = textureLoad;}
void controller::eventCheck( SDL_Event& e ){
	//flag_move = false;
	// Нажали
	if( e.type == SDL_KEYDOWN && e.key.repeat == 0 ) {
		switch( e.key.keysym.sym ) {
			case SDLK_a: movX -= G_speedShips; spriteY=180; vectX = -1; flag_move_a = true; break;
			case SDLK_d: movX += G_speedShips; spriteY=345; vectX = 1; flag_move_d = true;  break;
			//default: movX = 0;
		}
	}
	// Отпустили
	else if( e.type == SDL_KEYUP && e.key.repeat == 0 ) {
		switch( e.key.keysym.sym ) {
			case SDLK_a: movX += G_speedShips; flag_move_a = false; break;
			case SDLK_d: movX -= G_speedShips; flag_move_d = false; break;
			//default: movX = 0;
		}
	}
	if(!flag_move_a && !flag_move_d){movX = 0;}
}

void controller::move(){
	(flag_move_a || flag_move_d) ? starX += movX : movX = 0 ;
	if(starX >= SCREEN_WIDTH - spriteW + 30) {starX = (SCREEN_WIDTH - spriteW + 30);}
	if(starX <= -30) {starX = -30;}
}

void controller::render(){
	// если корабль движется
	if(movX != 0){ 	sprite.frame++; }
	
	sprite.iter = sprite.frame / G_speedAnimShips;
	if(sprite.iter >= 4) {sprite.frame = 0; sprite.iter = 0;}
	sprite.step = spriteW * sprite.iter;
	
	SDL_Rect currentClip = {sprite.step,spriteY,spriteW,120};
	texture.render( starX, Y_POS_SHIPS, &currentClip );
}

int controller::getPosX(){ return this->starX; }
int controller::getWidth(){ return this->spriteW; }
int controller::getVectX(){ return this->vectX; }
