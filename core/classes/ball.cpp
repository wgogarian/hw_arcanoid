#include <SDL2\SDL.h>
#include <SDL2\SDL_ttf.h>
#include <SDL2\SDL_mixer.h>

#include <iostream>
#include <string>
#include <sstream>

#include "../prototypes/classes.h"

// различные Глобальные переменные для работы
extern const int SCREEN_WIDTH; // ширина экрана
extern const int SCREEN_HEIGHT; // высота экрана

extern int G_speedCannonball; // скорость полёта ядер ( начальные )
extern bool G_pause; // играем или нет // скорость полёта ядер ( начальные )

extern controller Ships; // Запуск класса корабля

extern Mix_Music *endGame;
extern Mix_Chunk *canonShot;
extern Mix_Chunk *hit;
extern Mix_Chunk *hitShip;
extern Mix_Chunk *splashWater;


// BALL
ball::ball(){
	posX = -150;
	posY = -150;
	vectX = 1;
	vectY = -1;
	rect = {0,0,16,16}; // спрайт и размеры мячика
	FLAY = false;
	sounfirstShot = false; // чтобы звуки ненакладывались друг на друга при 1м выстреле
	soundEndSplash = false; // ядро затануло
	controllerW = Ships.getWidth();
	flagEndGame = false;
}
ball::~ball(){}

void ball::loadTexture( LTexture& textureLoad ){texture = textureLoad;};

void ball::eventCheck( SDL_Event& e ){
	// Нажали на пробел и выстрелили
	if( e.type == SDL_KEYDOWN && e.key.repeat == 0 && e.key.keysym.sym == SDLK_SPACE && FLAY == false) {
		FLAY = true;
		posX = Ships.getPosX()+ controllerW / 2;
		posY = SCREEN_HEIGHT-65;
		vectX = Ships.getVectX();
		Mix_PlayChannel( -1, canonShot, 0 );
		sounfirstShot = true;
	}
}

void ball::move(){
	if (FLAY){
		posX += (G_speedCannonball * vectX);
		posY += (G_speedCannonball * vectY);
		
		if(posX <= 0){
			reversX(); 
			setPosXY(0,posY);
			Mix_PlayChannel( -1, hit, 0 );
		}
		else if (posX >= SCREEN_WIDTH-rect.w) {
			reversX();
			setPosXY(SCREEN_WIDTH-rect.w,posY);
			Mix_PlayChannel( -1, hit, 0 );
		}	
		
		if(posY <= 0){
			reversY(); 
			setPosXY(posX,0);
			Mix_PlayChannel( -1, hit, 0 );
		}
		else if (posY >= SCREEN_HEIGHT-rect.h) {
			reversY();
			setPosXY(posX,SCREEN_HEIGHT-rect.h);
			Mix_PlayChannel( -1, hit, 0 );
		}
		
		if( posY > SCREEN_HEIGHT-60 ){
			// Проверка паподания в корабль
			if(posX+rect.w >= Ships.getPosX() && posX <= Ships.getPosX()+controllerW && !flagEndGame) {
				setPosXY(posX,SCREEN_HEIGHT-61);
				reversY();
				Mix_PlayChannel( -1, hitShip, 0 );
			} else {
				if(!soundEndSplash) {Mix_PlayChannel(-1, splashWater, 0 ); soundEndSplash = true;}
				flagEndGame = true;
			}
			
			if (posY > SCREEN_HEIGHT-20) {
				G_pause = true;
				Mix_PlayMusic(endGame, 1 );
			}
		}
	}
};

void ball::render(){texture.render( posX,posY, &rect );};
void ball::reversX(){ vectX *= -1; };
void ball::reversY(){ vectY *= -1; };
void ball::restart(){ 
	posX = -150;
	posY = -150;
	vectX = 1;
	vectY = -1;
	FLAY = false;
	soundEndSplash = false; // ядро затануло
	flagEndGame = false;
};
void ball::setPosXY(int x, int y){  posX = x;	posY = y;};
int *ball::getVectorXY(){ vectArr[0] = vectX; vectArr[1] = vectY; return vectArr; };
int ball::getPosX(){ return posX; };
int ball::getPosY(){ return posY; };