#include <SDL2\SDL.h>
#include <SDL2\SDL_ttf.h>
#include <SDL2\SDL_mixer.h>

#include <iostream>
#include <string>
#include <sstream>

#include "../prototypes/classes.h"

extern int random(int low, int high);

// различные Глобальные переменные для работы
extern const int SCREEN_WIDTH; // ширина экрана

extern int G_box_view;
extern int G_speedCannonball; // скорость полёта ядер ( начальные )
extern int G_maxLine; 
extern int G_maxChest;
extern int G_startLineX;
extern int G_startLineY;

extern struct chestInfoS chestInfo; // скорость движения коробля ( начальные )
extern LTexture TSСhest; // Сундуки
extern Mix_Chunk *chestEnd; //  Звук уничтоженного ящика

// для анимации спрайтов сундуков
struct chestInfoS{
	int sumSprite = 13; // Количество спрайтов в картинке
	int spriteStepX = 50; // Шаг внутри спрайта ( в лево )
	int spriteStepY = 0; // Шаг внутри спрайта ( в низ )
	int spriteW = 50; // Ширина одного спрайта
	int spriteH = 40; // Высота одного спрайта
	int startLineX = G_startLineX; // Начало первой линии по  X 
	int startLineY = G_startLineY; // Начало первой линии по  Y
	int maxLine = G_maxLine; // Максимум Линий сундуков
	int maxChest = G_maxChest; // Максимум сундуков в 1й линии
} chestInfo;

// ЯЩИКИ
chest::chest(){
	lifeChests = 0;
	// Заполним позиции для спрайта ящиков
	for (size_t i = 0; i < chestInfo.sumSprite; i++ ){
		SpriteChest[i].x = i * chestInfo.spriteStepX;
		SpriteChest[i].y = chestInfo.spriteStepY;
		SpriteChest[i].w = chestInfo.spriteW;
		SpriteChest[i].h = chestInfo.spriteH;
	}
};
chest::~chest(){};

void chest::init(int getLineChest, int getSumLineChest){
	lineChest = (getLineChest > chestInfo.maxLine)?chestInfo.maxLine:getLineChest;
	sumLineChest = (getSumLineChest > chestInfo.maxChest)?chestInfo.maxChest:getSumLineChest;
	lifeChests = lineChest*sumLineChest;
	stepChestX = (SCREEN_WIDTH-(chestInfo.startLineX*2) ) / sumLineChest;
	stepChestY = chestInfo.spriteH + 12;
	//this->clear(); // память то почистить надо )
	// выделим память для ящиков в виде 2 мерного массива ( линии - колонки )
	chestD = new chestStruct *[lineChest];
	for(size_t i = 0; i < lineChest; ++i) chestD[i] = new chestStruct[sumLineChest];
	// Заполним ящики всяким
	for (int i = 0; i < lineChest; i++) { // Линии
		for (int j = 0; j < sumLineChest; j++){ // Сундуки
			chestD[i][j].x = (stepChestX * j) + chestInfo.startLineX; // Смещение СПРАЙТА
			chestD[i][j].y = (stepChestY * i) + chestInfo.startLineY; // Смещение ЛИНИИ
			chestD[i][j].sprite = random(0,chestInfo.sumSprite-1); // номер картинки в спрайте
			chestD[i][j].life = true; // Жив ли ящик
		}
	}
};

void chest::colision(ball *cannon){ cannonB = cannon;}

void chest::render(){
	 // текущая позиция ядра
	int canX = (*cannonB).getPosX();
	int canY = (*cannonB).getPosY();
	bool collisiotTest = false;
	bool controlRevers = false;
	// вычислять ТОЛЬКО если шарик в нужнем поле ( диапазона )
	if( canY <= (chestInfo.startLineY+(stepChestY*lineChest)) ) {
		collisiotTest = true;
	}
	// рисуем ящечки :)
	for (int i=0; i < lineChest; i++) {
		for (int j=0; j < sumLineChest; j++){
			if(chestD[i][j].life){ // Если ящик живой 
				int boxX = chestD[i][j].x;
				int boxY = chestD[i][j].y;

				TSСhest.render( boxX, boxY, &SpriteChest[chestD[i][j].sprite] );
				
				if(collisiotTest && canY <= boxY+chestInfo.spriteH && (canY+16) >= boxY && canX+16 >= boxX && canX <= boxX+chestInfo.spriteW){ // если колизия случилась
					int *vectBalXY = (*cannonB).getVectorXY();
					int backBallX = canX - (G_speedCannonball*vectBalXY[0]);

					if (backBallX+16 > boxX && backBallX < boxX+chestInfo.spriteW && !controlRevers){
						(*cannonB).reversY(); controlRevers = true;
					}
					else {
						(*cannonB).reversX(); controlRevers = true;
					}
					++G_box_view;
					--lifeChests;
					Mix_PlayChannel( -1, chestEnd, 0 );
					chestD[i][j].life = false;
				}
			}
		}
	}
};

int chest::getSumChest(){ return lifeChests; }
void chest::clear(){
	for(size_t i = 0; i < lineChest; ++i) delete[] chestD[i];
	delete[] chestD;
};