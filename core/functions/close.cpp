#include <SDL2\SDL.h>
#include <SDL2\SDL_image.h>
#include <SDL2\SDL_ttf.h>
#include <SDL2\SDL_mixer.h>

#include <iostream>

#include "../prototypes/classes.h"

// fcn
extern void close();

// различные Глобальные переменные для работы
extern SDL_Window* gWindow; //The window we'll be rendering to
extern SDL_Renderer* gRenderer; //The window renderer

//Шрифт
extern TTF_Font* gFont;
extern TTF_Font* gFontBig;

//Текстуры
extern LTexture TSWater; // Вода
extern LTexture TSBack; // бек граунд
extern LTexture TSBackLife; // бек Движущейся
extern LTexture TSShips; // кораблик
extern LTexture TSСhest; // Сундуки
extern LTexture TSCannonBall; // пушачное ядро
extern LTexture gFPSTextTexture; // текст

//Музыка
extern Mix_Music *backgroundMusic;
extern Mix_Music *endGame;
extern Mix_Music *lvlUp;

//Семплы
extern Mix_Chunk *canonShot;
extern Mix_Chunk *hit;
extern Mix_Chunk *hitShip;
extern Mix_Chunk *splashWater;
extern Mix_Chunk *chestEnd;

extern chest Chests; // Класс сундуков

// Очистим память и всё что можно очистить.
void close() {
	//Free loaded image
	TSWater.free();
	TSBack.free();
	TSBackLife.free();
	TSShips.free();
	TSCannonBall.free();
	TSСhest.free();
	gFPSTextTexture.free();
	
	//Free global font
	TTF_CloseFont( gFont );
	TTF_CloseFont( gFontBig );
	gFont = NULL;
	gFontBig = NULL;
	
    //Free the sound effects
    Mix_FreeChunk( canonShot );  canonShot = NULL;
    Mix_FreeChunk( hit ); hit = NULL;
    Mix_FreeChunk( hitShip ); hitShip = NULL;
    Mix_FreeChunk( splashWater ); splashWater = NULL;
	
    //Free the music
    Mix_FreeMusic( backgroundMusic );  backgroundMusic = NULL;
    Mix_FreeMusic( endGame );  endGame = NULL;
    Mix_FreeMusic( lvlUp );  lvlUp = NULL;

	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	SDL_Quit();
    IMG_Quit();
	TTF_Quit();
	Mix_Quit();
	
	Chests.clear();
}